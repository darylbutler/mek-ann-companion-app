﻿using ExcelDataReader;
using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using TwinCAT.Ads;

namespace MEK_ANN_Controller
{
	public partial class LiveView : Form
	{
		// Testing
		private static readonly Random rand = new Random();

		private const bool Testing =
#if DEBUG
			true;

#else
			false;

#endif

		// -- Axis Colors
		private Color Plot_Background = Color.White;

		private OxyColor History_Flow_Color = OxyColors.Brown;
		private OxyColor History_Conc_Color = OxyColors.CadetBlue;
		private OxyColor Plot_Live_Line = OxyColors.IndianRed;
		private OxyColor Plot_Ignore_Line = OxyColors.SlateGray;

		private readonly OxyColor[] Prediction_Colors = {
			OxyColors.Magenta,
			OxyColors.SeaGreen,
			OxyColors.CornflowerBlue,
			OxyColors.PaleVioletRed,
			OxyColors.Green,
			OxyColors.LightBlue,
			OxyColors.MediumOrchid,
			OxyColors.DarkRed,
			OxyColors.LightGreen,
			OxyColors.Blue,
			OxyColors.Orange,
			OxyColors.IndianRed,
			OxyColors.GreenYellow,
			OxyColors.Navy,
			OxyColors.Tan,
			OxyColors.Black,
			OxyColors.Gray
		};

		// -- Plot Stuff
		private readonly OxyPlot.WindowsForms.PlotView Plot = new OxyPlot.WindowsForms.PlotView();

		private readonly PlotModel Data = new PlotModel();
		private LineSeries HistoryFlowSeries, HistoryConcSeries;

		// -- Twincat ADS Client
		private TcAdsClient ADS;

		private bool RunningPLC, TwinCat3;
		private readonly int port;
		private AdsStream RequestStream, RecieveStream;
		private Timer UpdateTimer;
		private DateTime LastRead;
		private uint ANNWatchdog = 0;

		// -- Local Data -- To be read from PLC
		private int History_Length = 250;

		private int Prediction_Length = 200;
		private int Ignore_Space = 49;
		private readonly int Prediction_Count = 11;
		private List<double> History_Flow = new List<double>();
		private List<double> History_Conc = new List<double>();
		private List<List<double>> Predictions = new List<List<double>>();

		private string DumpANNDataPath = string.Empty;
		private bool DumpAnnData = false;

		// CSV Data Saving
		private string SavingDataPath = string.Empty;

		private bool SavingDataStarted = false;

		private readonly List<string> SavingDataBuffer = new List<string>(500);

		public LiveView(int tc3) {
			InitializeComponent();
			port = tc3;
			TwinCat3 = port == 851;

			if (port == 851) {
				TCLabel.Text = "TwinCAT 3";
				TCLabel.BackColor = Color.CornflowerBlue;
			}
			else if (port == 801) {
				TCLabel.Text = "TwinCAT 2";
				TCLabel.BackColor = Color.LawnGreen;
			}
			else {
				TCLabel.Text = "Unknown";
			}

			// -- Connect to PLC
			TryToConnectToPLC();

			// -- Create the Plot
			PlotContainer.Controls.Add(Plot);
			Plot.Dock = DockStyle.Fill;
			Plot.BackColor = Plot_Background;

			// -- Fill the model
			Data.Title = "Live View of EM MEK ANN";
			CreateSeries();
			if (RunningPLC) {
				UpdateFromPLC();
			}
			else {
				InitalTestFill();
			}

			// -- Create the update timer
			SetupLiveUpdateTimer();

			// -- Update the UI
			UpdatePlotView();
			PopulateSeriesVisibilityListBox();
			PLCConnectedBtn.Checked = RunningPLC;
			ClearCSVData();
		}

		// -- Testing Functions (Data Generation) -------------------------------------------------
		private void InitalTestFill() {
			CreateSeries();

			// -- History and Conc -----------------------------------------------------------
			History_Flow = new List<double>(History_Length);
			History_Conc = new List<double>(History_Length);
			for (int i = 0; i < History_Length; i++) {
				History_Flow.Add(GenNewFlow());
				History_Conc.Add(GenNewConc());
			}

			// -- Generate concentrations for 11 possible future flows
			Predictions = new List<List<double>>(Prediction_Count);
			for (int i = 0; i < Prediction_Count; i++) {
				var prediction = new List<double>(Prediction_Length);
				for (int n = 0; n < Prediction_Length; n++) {
					prediction.Add(GenNewConc());
				}
				Predictions.Add(prediction);
			}
		}

		private double GenNewFlow() {
			return 0.35d + (1.6d * 0.35d * rand.NextDouble());
		}

		private double GenNewConc() {
			return 95.0d + (5.0d * rand.NextDouble());
		}

		private void UpdateWithNewTestData() {
			Debug.Assert(History_Flow.Count == History_Length
						 && History_Conc.Count == History_Length);

			History_Flow.RemoveAt(0);
			History_Conc.RemoveAt(0);
			History_Flow.Add(GenNewFlow());
			History_Conc.Add(GenNewConc());

			// -- Generate concentrations for 11 possible future flows
			Debug.Assert(Predictions.Count == Prediction_Count);
			for (int i = 0; i < Prediction_Count; i++) {
				Debug.Assert(Predictions[i].Count == Prediction_Length);
				for (int n = 0; n < Prediction_Length; n++) {
					Predictions[i][n] = GenNewConc();
				}
			}
		}

		// -- PLC Functions (Data Gathering) ------------------------------------------------------
		private void TryToConnectToPLC() {
			try {
				ADS = new TcAdsClient();
				if (!Testing)
					ADS.Connect(port);
				RunningPLC = ADS.IsConnected && ADS.TryReadState(out StateInfo state) == AdsErrorCode.NoError && state.AdsState == AdsState.Run;
			}
			catch (Exception) {
				// No PLC here, use testing mode and cleanup
				RunningPLC = false;
				ADS.Dispose();
				ADS = null;
			}
		}

		private void UpdateFromPLC() {
			if (!RunningPLC || ADS?.IsConnected != true)
				return;

			var varPrefix = TwinCat3 ? "prgMEK_ANN" : string.Empty;

			History_Length = Convert.ToInt32(ADS.Read(varPrefix + ".MEK_HISTORY_LEN", typeof(ushort)));
			Prediction_Length = Convert.ToInt32(ADS.Read(varPrefix + ".MEK_PREDICTION_LEN", typeof(ushort)));
			Ignore_Space = Convert.ToInt32(ADS.Read(varPrefix + ".MEK_IGNORE_SPACE", typeof(ushort)));

			int ReadBytes =
						(3 * 4) + // 3 vars, so 3 responses
						(History_Length * 8) +  // History Flow
						(History_Length * 8) +  // History Conc
						(Prediction_Count * Prediction_Length * 8);  // All the Predicitons

			// -- Generate a new request if we need one
			if (RequestStream == null) {
				// Request 3 variables
				RequestStream = new AdsStream(3 * 12);
				var writer = new BinaryWriter(RequestStream);

				// History_Flow
				writer.Write((int)AdsReservedIndexGroups.SymbolValueByHandle);
				writer.Write(ADS.CreateVariableHandle("prgMEK_ANN.History_Flow"));
				writer.Write(History_Length * 8);
				// History_Conc
				writer.Write((int)AdsReservedIndexGroups.SymbolValueByHandle);
				writer.Write(ADS.CreateVariableHandle("prgMEK_ANN.History_Conc"));
				writer.Write(History_Length * 8);
				// Predictions Array
				writer.Write((int)AdsReservedIndexGroups.SymbolValueByHandle);
				writer.Write(ADS.CreateVariableHandle("prgMEK_ANN.FlowPredictionsP1"));
				writer.Write(Prediction_Count * Prediction_Length * 8);
			}

			if (RecieveStream == null)
				RecieveStream = new AdsStream(ReadBytes);
			RecieveStream.Position = 0;

			// -- Send request to PLC
			ADS.ReadWrite(0xF080, 3, RecieveStream, RequestStream);
			var reader = new BinaryReader(RecieveStream);

			// -- Check if all data was read correctly
			RecieveStream.Position = 0;
			int error_HistoryFlow = reader.ReadInt32();
			int error_HistoryConc = reader.ReadInt32();
			int error_Predictions = reader.ReadInt32();

			if (error_HistoryFlow != (int)AdsErrorCode.NoError) {
				ADS.Dispose();
				ToggleLiveUpdateTimer(true);
				MessageBox.Show("Error Polling PLC for Live View: Error Reading History Flow!", "Error Polling PLC", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if (error_HistoryConc != (int)AdsErrorCode.NoError) {
				ADS.Dispose();
				ToggleLiveUpdateTimer(true);
				MessageBox.Show("Error Polling PLC for Live View: Error Reading History Conc!", "Error Polling PLC", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if (error_Predictions != (int)AdsErrorCode.NoError) {
				ADS.Dispose();
				ToggleLiveUpdateTimer(true);
				MessageBox.Show("Error Polling PLC for Live View: Error Reading Predictions!", "Error Polling PLC", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			// -- Copy output to local storage structures
			// History Flow
			History_Flow.Clear();
			for (int i = 0; i < History_Length; i++) {
				History_Flow.Add(reader.ReadDouble());
			}
			// History Conc
			History_Conc.Clear();
			for (int i = 0; i < History_Length; i++) {
				History_Conc.Add(reader.ReadDouble());
			}
			// Predictions
			Predictions.Clear();
			for (int i = 0; i < Prediction_Count; i++) {
				var l = new List<double>(Prediction_Length);
				for (int n = 0; n < Prediction_Length; n++) {
					l.Add(reader.ReadDouble());
				}
				Predictions.Add(l);
			}

			// Save the data to file if requested
			if (DumpAnnData) {
				//var flows = new List<double> { 0.480d, 0.592d, 0.704d, 0.816d, 0.928d, 1.040d, 1.152d, 1.264d, 1.376d, 1.488d, 1.60d };
				var flows = (double[])ADS.Read("prgMEK_ANN.PossFlowsP1", typeof(double[]));

				using (var io = new StreamWriter(DumpANNDataPath, false)) {
					io.WriteLine("History Flow: {0}", string.Join(", ", History_Flow));
					io.WriteLine("History Conc: {0}", string.Join(", ", History_Conc));

					for (int i = 0; i < Prediction_Count; i++) {
						io.WriteLine("Prediction {0:0.00}-> {1}", flows[i], string.Join(", ", Predictions[i]));
					}
				}
				DumpAnnData = GetANNDataBtn.Checked = false;
			}
		}

		// -- Plot / Series Management ------------------------------------------------------------
		private void CreateSeries() {
			Data.Series.Clear();

			// -- History Series
			HistoryFlowSeries = new LineSeries() {
				Title = "Flow History",
				Color = History_Flow_Color,
				Smooth = true,
				IsVisible = false
			};
			HistoryConcSeries = new LineSeries() {
				Title = "Conc History",
				Color = History_Conc_Color
			};
			Data.Series.Add(HistoryFlowSeries);
			Data.Series.Add(HistoryConcSeries);

			// -- Predictions
			for (int i = 0; i < Prediction_Count; i++) {
				Data.Series.Add(new LineSeries() {
					Title = string.Format("Prediction {0}", i + 1),
					Color = Prediction_Colors[i % (Prediction_Colors.Length - 1)]
				});
			}

			// -- Add Annotations
			Data.Annotations.Clear();
			// Show the Live Line
			Data.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() {
				Type = OxyPlot.Annotations.LineAnnotationType.Vertical,
				X = 0,
				Color = Plot_Live_Line,
				Text = "Live Data"
			});
			// Ignore Space Line
			Data.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() {
				Type = OxyPlot.Annotations.LineAnnotationType.Vertical,
				X = 0 - Ignore_Space,
				Color = Plot_Ignore_Line,
				Text = "Ignore Area"
			});
		}

		private LineSeries GetPredictionsSeries(int i) {
			if (i < 0 || i + 2 >= Data.Series.Count)
				return null;
			return Data.Series[2 + i] as LineSeries;
		}

		private void UpdatePlotView() {
			// -- Update Points to History Flows and Conc arrays
			HistoryFlowSeries.Points.Clear();
			HistoryConcSeries.Points.Clear();
			for (int i = 0; i < History_Length; i++) {
				var x = i - History_Length + 1;
				HistoryFlowSeries.Points.Add(new DataPoint(x, History_Flow[i]));
				HistoryConcSeries.Points.Add(new DataPoint(x, History_Conc[i]));
			}
			// -- Update Prediction points to local data
			for (int i = 0; i < Prediction_Count; i++) {
				var s = GetPredictionsSeries(i);
				s.Points.Clear();
				for (int n = 0; n < Prediction_Length; n++) {
					s.Points.Add(new DataPoint(n + 1, Predictions[i][n]));
				}
			}

			// -- Update the visual representation
			if (Plot.Model != Data)
				Plot.Model = Data;
			else
				Plot.Invalidate();
		}

		// -- Series Visibility UI / Logic --------------------------------------------------------
		private void PopulateSeriesVisibilityListBox() {
			// -- Setup Series Visibility box
			SeriesVisibilityOptionsList.Items.Add("History Flow Visible", HistoryFlowSeries.IsVisible);
			SeriesVisibilityOptionsList.Items.Add("History Conc Visible", HistoryConcSeries.IsVisible);
			for (int i = 0; i < Prediction_Count; i++) {
				SeriesVisibilityOptionsList.Items.Add(
					string.Format("Prediction {0:00} Visible", i + 1),
					GetPredictionsSeries(i).IsVisible);
			}

			SeriesVisibilityOptionsList.ItemCheck += (s, e) => {
				if (e.Index < 0 || (SeriesVisibilityOptionsList.Tag != null && (bool)SeriesVisibilityOptionsList.Tag)) return;

				var item = SeriesVisibilityOptionsList.Items[e.Index].ToString();
				if (item.StartsWith("History Flow")) {
					HistoryFlowSeries.IsVisible = (e.NewValue == CheckState.Checked);
					Data.ResetAllAxes();
				}
				else if (item.StartsWith("History Conc")) {
					HistoryConcSeries.IsVisible = (e.NewValue == CheckState.Checked);
				}
				else if (item.StartsWith("Prediction") && int.TryParse(item.Substring(11, 2), out int num)) {
					var series = GetPredictionsSeries(num - 1);
					if (series != null) {
						if (ModifierKeys == Keys.Shift) { // Deselect all predictions but this one
							SeriesVisibilityOptionsList.Tag = true;
							for (int i = 0; i < Prediction_Count; i++) {
								GetPredictionsSeries(i).IsVisible = (i == num - 1) && e.NewValue == CheckState.Checked;
								SeriesVisibilityOptionsList.SetItemChecked(i + 2, (i == num - 1) && e.NewValue == CheckState.Checked);
							}
							SeriesVisibilityOptionsList.Tag = false;
						}
						else {
							// Simply toggle this one visible / invisible
							series.IsVisible = (e.NewValue == CheckState.Checked);
						}
					}
				}
				Plot.Invalidate();
			};
		}

		// -- Live Updating -----------------------------------------------------------------------
		private void SetupLiveUpdateTimer() {
			LiveUpdateBtn.Checked = true;
			ManualReadBtn.Enabled = false;
			UpdateTimer = new Timer() {
				Enabled = true,
				Interval = RunningPLC ? 50 : 5000   // 5s for test data
			};
			UpdateTimer.Tick += UpdateTimer_UpdateData;
		}

		private void UpdateTimer_UpdateData(object s, EventArgs e) {
			if (!this.Visible) return;

			if (RunningPLC) {
				if (CheckForANNRun()) {
					UpdateFromPLC();
				}
				else {
					return; // Don't do anything right now
				}
			}
			else {
				UpdateWithNewTestData();
			}
			LastRead = DateTime.Now;

			// Copy the newest data to the text box if enabled
			if (SavingDataStarted) {
				AddCSVData(LastRead, History_Flow.Last(), History_Conc.Last());
			}

			UpdatePlotView();
		}

		private bool CheckForANNRun() {
			try {
				var wd = Convert.ToUInt32(ADS.Read("prgMEK_ANN.uiAnnWatchDog", typeof(uint)));
				if (wd != ANNWatchdog) {
					ANNWatchdog = wd;
					return true;
				}
			}
			catch (Exception e) {
				// Twincat read error. Log error in messagebox but continue the timer
				CSVData.AppendText($"Error Polling TC for if ANN has run. Error: ${e.Message}");
			}
			return false;
		}

		private void ToggleLiveUpdateTimer(bool error = false) {
			UpdateTimer.Enabled = !error && !UpdateTimer.Enabled;
			LiveUpdateBtn.Checked = UpdateTimer.Enabled;
		}

		private void LiveUpdateBtn_Click(Object sender, EventArgs e) {
			// If we're not connected, try to connect again.
			if (!Testing && ADS?.IsConnected != true) {
				TryToConnectToPLC();
				UpdateFromPLC();
			}
			ToggleLiveUpdateTimer();
		}

		private void LiveUpdateBtn_CheckedChanged(Object sender, EventArgs e) {
			var btn = sender as CheckBox;
			if (btn.Checked) {
				btn.BackColor = Color.LightGreen;
				ManualReadBtn.Enabled = false;
			}
			else {
				btn.BackColor = SystemColors.ButtonFace;
				ManualReadBtn.Enabled = true;
			}
		}

		// -- Manual Reading (When Live Update is off) --------------------------------------------
		private void ManualReadBtn_Click(Object sender, EventArgs e) {
			if (UpdateTimer.Enabled)
				return; // Only allowed if not automatically updating

			if (RunningPLC) {
				UpdateFromPLC();
			}
			else {
				UpdateWithNewTestData();
			}
			UpdatePlotView();
		}

		// -- Data Saving -------------------------------------------------------------------------
		private void AddCSVData(DateTime dt, double flow, double conc) {
			const int MaxLines = 250;
			const int SaveAfterNumLines = 12 * 1;  // 12 lines per minute

			var text = string.Format("{0:yyyy-MM-dd HH:mm:ss.fff},{1:0.000000},{2:0.000000},\r\n", dt, flow, conc);

			if (CSVData.Lines.Length > MaxLines) {
				var l = CSVData.Lines.ToList();
				l.RemoveAt(1);
				CSVData.Lines = l.ToArray();
			}
			CSVData.AppendText(text);

			// -- Save the data to a file if selected
			if (SavingDataPath.Length > 0) {
				SavingDataBuffer.Add(text);
				if (SavingDataBuffer.Count > SaveAfterNumLines) {
					SaveStringsToFile(SavingDataBuffer, SavingDataPath);
					SavingDataBuffer.Clear();
				}
			}
		}

		private void SaveStringsToFile(IEnumerable<string> lines, string path, bool append = true) {
			if (!append && File.Exists(path)) {
				File.Delete(path);
			}

			using (var f = File.OpenWrite(path))
			using (var w = new StreamWriter(f)) {
				w.BaseStream.Seek(0, SeekOrigin.End);
				foreach (var line in lines) {
					w.WriteLine(line.Trim('\r', '\n'));
				}
			}
		}

		private void ClearCSVData() {
			CSVData.Text = "DateTime,MEK_Flow,MEK_Concentration,\r\n";
		}

		private void SaveCSVData() {
			using (var dlg = new SaveFileDialog() {
				Title = "Save EM MEK ANN History CSV",
				Filter = "CSV Files (*.csv)|*.csv|Excel Files (*.xls, *.xlsx)|*.xls;*.xlsx|All Files (*.*)|*.*",
				SupportMultiDottedExtensions = true
			}) {
				if (dlg.ShowDialog() == DialogResult.OK) {
					if (dlg.FileName.EndsWith(".xls") || dlg.FileName.EndsWith(".xlsx")) {
						MessageBox.Show("Sorry, Excel exporting not supported yet, but I'm planning on it!", "Not Supported!", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else {
						try {
							File.WriteAllText(dlg.FileName, CSVData.Text);
						}
						catch (Exception ex) {
							MessageBox.Show("Error During File Saving!\nError: " + ex.Message, "Error during CSV export!", MessageBoxButtons.OK, MessageBoxIcon.Error);
						}
					}
				}
			}
		}

		private void CopyCSVBtn_CheckedChanged(Object sender, EventArgs e) {
			var btn = sender as CheckBox;
			if (!SavingDataStarted) {
				btn.BackColor = Color.LightGreen;

				// -- Get a save path
				using (var dlg = new SaveFileDialog() {
					Title = "Select File to persist this data to (Or cancel to only store in memory)",
					Filter = "CSV Files (*.csv)|*.csv|All Files (*.*)|*.*",
					SupportMultiDottedExtensions = true
				}) {
					if (dlg.ShowDialog() == DialogResult.OK) {
						SavingDataPath = dlg.FileName;
						SaveStringsToFile(
							new string[] { "DateTime,MEK_Flow,MEK_Concentration,\r\n" },
							SavingDataPath,
							false);
					}
					else {
						SavingDataPath = string.Empty;  // Saving without path
					}
					SavingDataStarted = true;
				}
			}
			else {
				btn.BackColor = SystemColors.ButtonFace;

				// -- Save and Data that's buffered
				if (SavingDataPath.Length > 0 && SavingDataBuffer.Count > 0) {
					SaveStringsToFile(SavingDataBuffer.ToArray(), SavingDataPath);
					SavingDataBuffer.Clear();
				}
				SavingDataStarted = false;
			}
		}

		private void ClearCSVDataBtn_Click(Object sender, EventArgs e) {
			ClearCSVData();
		}

		private void SaveToFileBtn_Click(Object sender, EventArgs e) {
			SaveCSVData();
		}

		// -- Dumping ANN Data --------------------------------------------------------------------
		private void GetANNDataBtn_CheckedChanged(Object sender, EventArgs e) {
			if (GetANNDataBtn.Checked) {
				using (var dlg = new SaveFileDialog() {
					Title = "Save ANN data",
					Filter = "CSV Files (*.csv)|*.csv|All Files (*.*)|*.*",
					SupportMultiDottedExtensions = true
				}) {
					if (dlg.ShowDialog() == DialogResult.OK) {
						DumpANNDataPath = dlg.FileName;
						DumpAnnData = true;
					}
					else {
						DumpAnnData = GetANNDataBtn.Checked = false;
					}
				}
			}
			else {
				DumpAnnData = false;
			}
		}

		// -- Newer -------------------------------------------------------------------------------
		private void SimNeuralNetBtn_Click(Object sender, EventArgs e) {
			// -- Load the Biases to Sim
			var dlg = new OpenFileDialog() {
				Multiselect = false,
				Filter = "Excel Files (.xls, .xlsx)|*.xls;*.xlsx|All Files (*.*)|*.*"
			};
			if (dlg.ShowDialog() == DialogResult.OK) {
				// Clear the data
				ToggleLiveUpdateTimer(true);
				var ann = new NeuralNet {
					L1_Flows = new List<List<double>>(),
					L1_Conc = new List<List<double>>(),
					L1_Bias = new List<double>(),
					L2_Weights = new List<double>()
				};

				// Parse the Excel file
				using (var reader = ExcelReaderFactory.CreateReader(File.Open(dlg.FileName, FileMode.Open, FileAccess.Read))) {
					int sheet = 0;
					do {
						sheet++;
						switch (sheet) {
							case 1: // FlowWeights sheet
								reader.Read();  // skip 'Weights ->' line
								reader.Read();  // skip 'Neuron ' line
								while (reader.Read()) {
									var row = new List<double>(reader.FieldCount - 1);
									for (int i = 1; i < reader.FieldCount; ++i) {
										row.Add(reader.GetDouble(i));
									}
									ann.L1_Flows.Add(row);
								}
								break;

							case 2: // ConcWeights sheet
								reader.Read();  // skip 'Weights ->' line
								reader.Read();  // skip 'Neuron ' line
								while (reader.Read()) {
									var row = new List<double>(reader.FieldCount - 1);
									for (int i = 1; i < reader.FieldCount; ++i) {
										row.Add(reader.GetDouble(i));
									}
									ann.L1_Conc.Add(row);
								}
								break;

							case 3: // InputBias sheet
								reader.Read();  // skip 'Biases ->' line
								reader.Read();  // skip 'Neuron ' line
								while (reader.Read()) {
									ann.L1_Bias.Add(reader.GetDouble(1));
								}
								break;

							case 4: // OutputWeights sheet
								reader.Read();  // skip 'Weights ->' line
								reader.Read();  // skip 'Neuron ' line
								reader.Read();  // Line with values we want
								for (int i = 1; i < reader.FieldCount; ++i) {
									ann.L2_Weights.Add(reader.GetDouble(i));
								}

								break;

							case 5: // OutputBias sheet
								reader.Read();  // skip 'Bias ->' line
								reader.Read();  // skip 'Neuron ' line
								reader.Read();  // The line with the value we want
								ann.L2_Bias = reader.GetDouble(1);
								break;
						}
					} while (reader.NextResult());
				}

				ann.History_Conc = History_Conc;
				ann.History_Flow = History_Flow;
				for (int i = 0; i < ann.L1_Flows.Count; i++) {
					ann.L1_Flows[i].Reverse();
					ann.L1_Conc[i].Reverse();
				}
				ann.Run();

				// Graph the output
				History_Flow = new List<double>(ann.History_Flow);
				History_Conc = new List<double>(ann.History_Conc);
				Predictions = ann.Predictions;
				UpdatePlotView();
			}
		}

		private void LiveView_FormClosing(Object sender, FormClosingEventArgs e) {
			// Only hide the form on the close button
			e.Cancel = true;
			Visible = false;
		}
	}
}