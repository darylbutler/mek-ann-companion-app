﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;
using TwinCAT.Ads;

namespace MEK_ANN_Controller
{
	public partial class MainForm : Form
	{
		private LiveView LiveViewForm;

		private List<List<double>> L1_Flows = new List<List<double>>();
		private List<List<double>> L1_Conc = new List<List<double>>();
		private List<double> L1_Bias = new List<double>();
		private List<double> L2_Weights = new List<double>();
		private double L2_Bias;

		private readonly bool TwinCat3;
		private int port;
		private readonly List<double> TestData_Flow = new List<double>();
		private readonly List<double> TestData_Conc = new List<double>();

		public MainForm() {
			InitializeComponent();
			TwinCat3 = IsTwinCat3();
		}

		private void LoadBtn_Click(Object sender, EventArgs e) {
			var dlg = new OpenFileDialog() {
				Multiselect = false,
				Filter = "Excel Files (.xls, .xlsx)|*.xls;*.xlsx|All Files (*.*)|*.*"
			};
			if (dlg.ShowDialog() == DialogResult.OK) {
				// Clear the data
				L1_Flows = new List<List<double>>();
				L1_Conc = new List<List<double>>();
				L1_Bias = new List<double>();
				L2_Weights = new List<double>();

				// Parse the Excel file
				using (var reader = ExcelReaderFactory.CreateReader(File.Open(dlg.FileName, FileMode.Open, FileAccess.Read))) {
					int sheet = 0;
					do {
						sheet++;
						switch (sheet) {
							case 1: // FlowWeights sheet
								reader.Read();  // skip 'Weights ->' line
								reader.Read();  // skip 'Neuron ' line
								while (reader.Read()) {
									var row = new List<double>(reader.FieldCount - 1);
									for (int i = 1; i < reader.FieldCount; ++i) {
										row.Add(reader.GetDouble(i));
									}
									L1_Flows.Add(row);
								}
								break;

							case 2: // ConcWeights sheet
								reader.Read();  // skip 'Weights ->' line
								reader.Read();  // skip 'Neuron ' line
								while (reader.Read()) {
									var row = new List<double>(reader.FieldCount - 1);
									for (int i = 1; i < reader.FieldCount; ++i) {
										row.Add(reader.GetDouble(i));
									}
									L1_Conc.Add(row);
								}
								break;

							case 3: // InputBias sheet
								reader.Read();  // skip 'Biases ->' line
								reader.Read();  // skip 'Neuron ' line
								while (reader.Read()) {
									L1_Bias.Add(reader.GetDouble(1));
								}
								break;

							case 4: // OutputWeights sheet
								reader.Read();  // skip 'Weights ->' line
								reader.Read();  // skip 'Neuron ' line
								reader.Read();  // Line with values we want
								for (int i = 1; i < reader.FieldCount; ++i) {
									L2_Weights.Add(reader.GetDouble(i));
								}

								break;

							case 5: // OutputBias sheet
								reader.Read();  // skip 'Bias ->' line
								reader.Read();  // skip 'Neuron ' line
								reader.Read();  // The line with the value we want
								L2_Bias = reader.GetDouble(1);
								break;
						}
					} while (reader.NextResult());
				}
				LoadL1FlowWeightsGrid();
				LoadL1ConcWeightsGrid();
				LoadL1BiasesGrid();
				LoadL2WeightsGrid();
				LoadL2BiasGrid();
				StatusLabel.Text = "Weights loaded from file, waiting to be written to PLC";
			}
		}

		private bool IsTwinCat3() {
			var host = Dns.GetHostEntry(Dns.GetHostName());
			foreach (var ip in host.AddressList) {
				if (ip.GetAddressBytes()[0] == 10 && ip.GetAddressBytes()[1] == 8) {
					if (ip.GetAddressBytes()[2] == 12 || ip.GetAddressBytes()[2] == 13) {
						TCLabel.Text = "TwinCAT 2";
						TCLabel.BackColor = Color.LawnGreen;
						port = 801;
						return false;
					}
					if (ip.GetAddressBytes()[2] == 16 || ip.GetAddressBytes()[2] == 17 || ip.GetAddressBytes()[2] == 18) {
						TCLabel.Text = "TwinCAT 3";
						TCLabel.BackColor = Color.CornflowerBlue;
						port = 851;
						return true;
					}
				}

				if (ip.GetAddressBytes()[0] == 10 && ip.GetAddressBytes()[1] == 146 && ip.GetAddressBytes()[2] == 44) {
					// Dev PC
					TCLabel.Text = "TwinCAT 3";
					TCLabel.BackColor = Color.CornflowerBlue;
					port = 851;
					return true;
				}
			}
			TCLabel.Text = "Unknown";
			port = 0;
			return false;
		}

		private void LoadL1FlowWeightsGrid() {
			L1FlowWeightsGrid.Rows.Clear();
			L1FlowWeightsGrid.Columns.Clear();
			L1FlowWeightsGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
			L1FlowWeightsGrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;

			DataGridViewColumn[] columns = new DataGridViewColumn[L1_Flows[0].Count];
			for (int i = 1; i <= L1_Flows[0].Count; ++i) {
				//L1FlowWeightsGrid.Columns.Add(string.Format("L1FlowWeightColumn{0}", i), i.ToString());
				columns[i - 1] = new DataGridViewTextBoxColumn {
					Name = string.Format("L1FlowWeightColumn{0}", i),
					HeaderText = i.ToString()
				};
			}
			L1FlowWeightsGrid.Columns.AddRange(columns);

			var rows = new List<DataGridViewRow>();
			foreach (var rowVals in L1_Flows) {
				var row = new DataGridViewRow();
				foreach (var val in rowVals) {
					row.Cells.Add(new DataGridViewTextBoxCell() { Value = val });
				}
				rows.Add(row);
			}
			L1FlowWeightsGrid.Rows.AddRange(rows.ToArray());

			L1FlowWeightsGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
			L1FlowWeightsGrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
		}

		private void LoadL1ConcWeightsGrid() {
			L1ConcWeightsGrid.Rows.Clear();
			L1ConcWeightsGrid.Columns.Clear();
			L1ConcWeightsGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
			L1ConcWeightsGrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;

			DataGridViewColumn[] columns = new DataGridViewColumn[L1_Conc[0].Count];
			for (int i = 1; i <= L1_Conc[0].Count; ++i) {
				columns[i - 1] = new DataGridViewTextBoxColumn {
					Name = string.Format("L1ConcWeightColumn{0}", i),
					HeaderText = i.ToString()
				};
			}
			L1ConcWeightsGrid.Columns.AddRange(columns);

			var rows = new List<DataGridViewRow>();
			foreach (var rowVals in L1_Conc) {
				var row = new DataGridViewRow();
				foreach (var val in rowVals) {
					row.Cells.Add(new DataGridViewTextBoxCell() { Value = val });
				}
				rows.Add(row);
			}
			L1ConcWeightsGrid.Rows.AddRange(rows.ToArray());

			L1ConcWeightsGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
			L1ConcWeightsGrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
		}

		private void LoadL1BiasesGrid() {
			L1BiasesGrid.Rows.Clear();
			L1BiasesGrid.Columns.Clear();

			L1BiasesGrid.Columns.Add("L1BiasesColumn1", "1");

			foreach (var rowVals in L1_Bias) {
				L1BiasesGrid.Rows.Add(rowVals);
			}
		}

		private void LoadL2WeightsGrid() {
			L2WeightsGrid.Rows.Clear();
			L2WeightsGrid.Columns.Clear();

			for (int i = 1; i <= L2_Weights.Count; ++i) {
				L2WeightsGrid.Columns.Add(string.Format("L2WeightsColumn{0}", i), i.ToString());
			}

			var row = new DataGridViewRow();
			foreach (var val in L2_Weights) {
				row.Cells.Add(new DataGridViewTextBoxCell() { Value = val });
			}
			L2WeightsGrid.Rows.Add(row);
		}

		private void LoadL2BiasGrid() {
			L2BiasGrid.Rows.Clear();
			L2BiasGrid.Columns.Clear();

			L2BiasGrid.Columns.Add("L2BiasColumn1", "Bias");
			L2BiasGrid.Rows.Add(L2_Bias);
		}

		private bool CanLoadParsedParams() {
			if (!double.TryParse(NormFlow_xOffset.Text, out Double test)) {
				MessageBox.Show("Cannot parse Normalize Flow xOffset text!");
				return false;
			}
			if (!double.TryParse(NormFlow_xGain.Text, out test)) {
				MessageBox.Show("Cannot parse Normalize Flow xGain text!");
				return false;
			}
			if (!double.TryParse(NormFlow_yMin.Text, out test)) {
				MessageBox.Show("Cannot parse Normalize Flow yMin text!");
				return false;
			}

			if (!double.TryParse(NormConc_xOffset.Text, out test)) {
				MessageBox.Show("Cannot parse Normalize Conc xOffset text!");
				return false;
			}
			if (!double.TryParse(NormConc_xGain.Text, out test)) {
				MessageBox.Show("Cannot parse Normalize Conc xGain text!");
				return false;
			}
			if (!double.TryParse(NormConc_yMin.Text, out test)) {
				MessageBox.Show("Cannot parse Normalize Conc yMin text!");
				return false;
			}

			if (!double.TryParse(UnnormConc_xOffset.Text, out test)) {
				MessageBox.Show("Cannot parse Un-normalize xOffset yMin text!");
				return false;
			}
			if (!double.TryParse(UnnormConc_xGain.Text, out test)) {
				MessageBox.Show("Cannot parse Un-normalize xGain yMin text!");
				return false;
			}
			if (!double.TryParse(UnnormConc_yMin.Text, out test)) {
				MessageBox.Show("Cannot parse Un-normalize yMin yMin text!");
				return false;
			}

			if (!double.TryParse(MEKMaxFlow.Text, out test) || test < 0.0d || test > 1.0d) {
				MessageBox.Show("Cannot parse MEK Valve Flow Clamp Max text!");
				return false;
			}
			if (!double.TryParse(MEKMinFlow.Text, out test) || test < 0.0d || test > 1.0d) {
				MessageBox.Show("Cannot parse MEK Valve Flow Clamp Min text!");
				return false;
			}

			if (!double.TryParse(DampeningLimit.Text, out test)) {
				MessageBox.Show("Cannot parse Dampening text!");
				return false;
			}

			if (!double.TryParse(RHO.Text, out test)) {
				MessageBox.Show("Cannot parse RHO text!");
				return false;
			}

			if (!double.TryParse(LowFlowPenalty.Text, out test)) {
				MessageBox.Show("Cannot parse Low Flow Penalty text!");
				return false;
			}

			if (!double.TryParse(HighFlowPenalty.Text, out test)) {
				MessageBox.Show("Cannot parse High Flow Penalty text!");
				return false;
			}

			return true;
		}

		private void WriteToPLCBtn_Click(Object sender, EventArgs e) {
			if (!CanLoadParsedParams())
				return;

			StatusLabel.Text = "Write Started...";
			Application.DoEvents();

			var adsClient = new TcAdsClient();
			adsClient.Connect(port);
			var varPrefix = TwinCat3 ? "prgMEK_ANN" : string.Empty;

			// -- Write L1 Flow Weights
			{
				var i = 1;
				foreach (var lst in L1_Flows) {
					//lst.Reverse();
					var ary = new List<double>(lst);
					ary.Reverse();
					adsClient.WriteSymbol(string.Format("{1}.gMEK_ANN_L1_FlowWeights[{0}]", i, varPrefix), ary.ToArray(), false);
					i++;
				}
			}

			// -- Write L1 Conc Weights
			{
				var i = 1;
				foreach (var lst in L1_Conc) {
					//lst.Reverse();
					var ary = new List<double>(lst);
					ary.Reverse();
					adsClient.WriteSymbol(string.Format("{1}.gMEK_ANN_L1_ConcWeights[{0}]", i, varPrefix), ary.ToArray(), false);
					i++;
				}
			}

			// -- Write L1 Biases
			{
				adsClient.WriteSymbol(varPrefix + ".gMEK_ANN_L1_Biases", L1_Bias.ToArray(), false);
			}

			// -- Write L2 Weights
			{
				adsClient.WriteSymbol(varPrefix + ".gMEK_ANN_L2_InputWeights", L2_Weights.ToArray(), false);
			}

			// -- Write L2 Bias
			{
				adsClient.WriteSymbol(varPrefix + ".gMEK_ANN_L2_InputBias", L2_Bias, false);
			}

			// -- Write Normalizations
			adsClient.WriteSymbol(varPrefix + ".glrMEK_ANN_L1_Flow_xOffset", double.Parse(NormFlow_xOffset.Text), false);
			adsClient.WriteSymbol(varPrefix + ".glrMEK_ANN_L1_Flow_xGain", double.Parse(NormFlow_xGain.Text), false);
			adsClient.WriteSymbol(varPrefix + ".glrMEK_ANN_L1_Flow_yMin", double.Parse(NormFlow_yMin.Text), false);
			adsClient.WriteSymbol(varPrefix + ".glrMEK_ANN_L1_Conc_xOffset", double.Parse(NormConc_xOffset.Text), false);
			adsClient.WriteSymbol(varPrefix + ".glrMEK_ANN_L1_Conc_xGain", double.Parse(NormConc_xGain.Text), false);
			adsClient.WriteSymbol(varPrefix + ".glrMEK_ANN_L1_Conc_yMin", double.Parse(NormConc_yMin.Text), false);
			adsClient.WriteSymbol(varPrefix + ".glrMEK_ANN_L2_Out_xOffset", double.Parse(UnnormConc_xOffset.Text), false);
			adsClient.WriteSymbol(varPrefix + ".glrMEK_ANN_L2_Out_xGain", double.Parse(UnnormConc_xGain.Text), false);
			adsClient.WriteSymbol(varPrefix + ".glrMEK_ANN_L2_Out_yMin", double.Parse(UnnormConc_yMin.Text), false);
			adsClient.WriteSymbol("prgMEK_ANN.lrANNDampeningMaxDev", double.Parse(DampeningLimit.Text), false);
			adsClient.WriteSymbol("prgMEK_ANN.lrRHO", double.Parse(RHO.Text), false);
			adsClient.WriteSymbol("prgMEK_ANN.uiSkipFirstPredictions", ushort.Parse(SkipPredictions.Text), false);
			adsClient.WriteSymbol("prgMEK_ANN.lrPenaltyHighCostFunc", double.Parse(HighFlowPenalty.Text), false);
			adsClient.WriteSymbol("prgMEK_ANN.lrPenaltyLowCostFunc", double.Parse(LowFlowPenalty.Text), false);
			adsClient.WriteSymbol(varPrefix + ".glrMaxFlowPercent", double.Parse(MEKMaxFlow.Text), false);
			adsClient.WriteSymbol(varPrefix + ".glrMinFlowPercent", double.Parse(MEKMinFlow.Text), false);

			adsClient.WriteSymbol("prgMEK_ANN.bAlwaysANN", AlwaysANN.Checked, false);
			adsClient.WriteSymbol("prgMEK_ANN.bUsingCorrectedPred", UsingCorrectedPredictions.Checked, false);

			if (TwinCat3) {
				adsClient.WriteSymbol("prg_MOD_STA02_MP.stHmiToPlc_ProductionParameterMP.rConcentrationSetpoint", float.Parse(ConcentrationSP.Text), false);
				adsClient.WriteSymbol("prgMEK_ANN.fbiErrorCorrection.uiCorrectionsToAvg", ushort.Parse(CorrectionsToAverage.Text), false);
			}
			else {
				adsClient.WriteSymbol("prgSta02EX1_MP.stHmiToPlc_ProcessParameterMP.rConcentrationSetpoint", float.Parse(ConcentrationSP.Text), false);
				adsClient.WriteSymbol("prgMEK_ANN.fbiErrorCorrection.iCorrectionsToAvg", short.Parse(CorrectionsToAverage.Text), false);
			}

			adsClient.Dispose();
			StatusLabel.Text = "Weights updated in PLC successfully";
		}

		private void ReadFromPLCBtn_Click(Object sender, EventArgs e) {
			StatusLabel.Text = "Read Started...";
			Application.DoEvents();

			var adsClient = new TcAdsClient();
			adsClient.Connect(port);
			var varPrefix = TwinCat3 ? "prgMEK_ANN" : string.Empty;

			// -- Read Params
			NormFlow_xOffset.Text = adsClient.Read(varPrefix + ".glrMEK_ANN_L1_Flow_xOffset", typeof(double)).ToString();
			NormFlow_xGain.Text = adsClient.Read(varPrefix + ".glrMEK_ANN_L1_Flow_xGain", typeof(double)).ToString();
			NormFlow_yMin.Text = adsClient.Read(varPrefix + ".glrMEK_ANN_L1_Flow_yMin", typeof(double)).ToString();
			NormConc_xOffset.Text = adsClient.Read(varPrefix + ".glrMEK_ANN_L1_Conc_xOffset", typeof(double)).ToString();
			NormConc_xGain.Text = adsClient.Read(varPrefix + ".glrMEK_ANN_L1_Conc_xGain", typeof(double)).ToString();
			NormConc_yMin.Text = adsClient.Read(varPrefix + ".glrMEK_ANN_L1_Conc_yMin", typeof(double)).ToString();
			UnnormConc_xOffset.Text = adsClient.Read(varPrefix + ".glrMEK_ANN_L2_Out_xOffset", typeof(double)).ToString();
			UnnormConc_xGain.Text = adsClient.Read(varPrefix + ".glrMEK_ANN_L2_Out_xGain", typeof(double)).ToString();
			UnnormConc_yMin.Text = adsClient.Read(varPrefix + ".glrMEK_ANN_L2_Out_yMin", typeof(double)).ToString();

			MEKMaxFlow.Text = adsClient.Read(varPrefix + ".glrMaxFlowPercent", typeof(double)).ToString();
			MEKMinFlow.Text = adsClient.Read(varPrefix + ".glrMinFlowPercent", typeof(double)).ToString();

			DampeningLimit.Text = adsClient.Read("prgMEK_ANN.lrANNDampeningMaxDev", typeof(double)).ToString();
			RHO.Text = adsClient.Read("prgMEK_ANN.lrRHO", typeof(double)).ToString();
			SkipPredictions.Text = adsClient.Read("prgMEK_ANN.uiSkipFirstPredictions", typeof(ushort)).ToString();
			LowFlowPenalty.Text = adsClient.Read("prgMEK_ANN.lrPenaltyLowCostFunc", typeof(double)).ToString();
			HighFlowPenalty.Text = adsClient.Read("prgMEK_ANN.lrPenaltyHighCostFunc", typeof(double)).ToString();
			UsingCorrectedPredictions.Checked = (bool)adsClient.Read("prgMEK_ANN.bUsingCorrectedPred", typeof(bool));

			AlwaysANN.Checked = (bool)adsClient.Read("prgMEK_ANN.bAlwaysANN", typeof(bool));
			if (TwinCat3)
				CorrectionsToAverage.Text = adsClient.Read("prgMEK_ANN.fbiErrorCorrection.uiCorrectionsToAvg", typeof(ushort)).ToString();
			else
				CorrectionsToAverage.Text = adsClient.Read("prgMEK_ANN.fbiErrorCorrection.iCorrectionsToAvg", typeof(short)).ToString();
			ConcentrationSP.Text = adsClient.Read(TwinCat3 ? "prg_MOD_STA02_MP.stHmiToPlc_ProductionParameterMP.rConcentrationSetpoint" : "prgSta02EX1_MP.stHmiToPlc_ProcessParameterMP.rConcentrationSetpoint", typeof(float)).ToString();

			// -- L1 Weights
			// Read the constants to know how big the weight arrays are
			var neurons = Convert.ToInt32(adsClient.Read(varPrefix + ".MEK_L1_NEURON_COUNT", typeof(ushort)));
			var cells = Convert.ToInt32(adsClient.Read(varPrefix + ".MEK_CELL_WEIGHT_COUNT", typeof(ushort)));

			// -- Read L1 Flow Weights
			{
				L1_Flows = new List<List<double>>(neurons);
				var stream = new AdsStream(cells * neurons * 8);
				adsClient.Read(adsClient.CreateVariableHandle(varPrefix + ".gMEK_ANN_L1_FlowWeights"), stream);
				var reader = new BinaryReader(stream);

				for (int neuron = 0; neuron < neurons; neuron++) {
					var weights = new List<double>(cells);
					for (int cell = 0; cell < cells; cell++) {
						weights.Add(reader.ReadDouble());
					}
					weights.Reverse();
					L1_Flows.Add(weights);
				}
				Debug.Assert(stream.Position == (neurons * cells * 8), "L1_Flows Stream wasn't read correctly!");
			}
			// -- Read L1 Conc Weights
			{
				L1_Conc = new List<List<double>>(neurons);
				var stream = new AdsStream(cells * neurons * 8);
				adsClient.Read(adsClient.CreateVariableHandle(varPrefix + ".gMEK_ANN_L1_ConcWeights"), stream);
				var reader = new BinaryReader(stream);

				for (int neuron = 0; neuron < neurons; neuron++) {
					var weights = new List<double>(cells);
					for (int cell = 0; cell < cells; cell++) {
						weights.Add(reader.ReadDouble());
					}
					weights.Reverse();
					L1_Conc.Add(weights);
				}
				Debug.Assert(stream.Position == (neurons * cells * 8), "L1_Conc Stream Wasn't read correctly!");
			}

			// -- Read L1 Biases
			{
				L1_Bias = new List<double>(neurons);
				var stream = new AdsStream(cells * neurons * 8);
				adsClient.Read(adsClient.CreateVariableHandle(varPrefix + ".gMEK_ANN_L1_Biases"), stream);
				var reader = new BinaryReader(stream);

				for (int neuron = 0; neuron < neurons; neuron++) {
					var val = reader.ReadDouble();
					L1_Bias.Add(val);
				}
			}

			// -- Write L2 Weights
			{
				L2_Weights = new List<double>(neurons);
				var stream = new AdsStream(cells * neurons * 8);
				adsClient.Read(adsClient.CreateVariableHandle(varPrefix + ".gMEK_ANN_L2_InputWeights"), stream);
				var reader = new BinaryReader(stream);

				for (int neuron = 0; neuron < neurons; neuron++) {
					var val = reader.ReadDouble();
					L2_Weights.Add(val);
				}
			}

			// -- Write L2 Bias
			{
				L2_Bias = Convert.ToDouble(adsClient.Read(varPrefix + ".gMEK_ANN_L2_InputBias", typeof(double)));
			}

			// Refresh the UI
			LoadL1FlowWeightsGrid();
			LoadL1ConcWeightsGrid();
			LoadL1BiasesGrid();
			LoadL2WeightsGrid();
			LoadL2BiasGrid();

			adsClient.Dispose();

			StatusLabel.Text = "PLC Variables read";
		}

		private void LiveViewBtn_Click(Object sender, EventArgs e) {
			if (LiveViewForm == null || ModifierKeys == Keys.Shift) {
				LiveViewForm?.Close();
				LiveViewForm = new LiveView(port);
				LiveViewForm.Show();
			}
			else if (LiveViewForm.Visible) {
				LiveViewForm.Visible = false;
			}
			else {
				LiveViewForm.Visible = true;
				LiveViewForm.BringToFront();
			}
		}

		private void DefaultParams6Minus_Click(Object sender, EventArgs e) {
			AlwaysANN.Checked = true;
			UsingCorrectedPredictions.Checked = true;
			ConcentrationSP.Text = "95.2";
			CorrectionsToAverage.Text = "12";
			DampeningLimit.Text = "0.1";
			LowFlowPenalty.Text = "1.0";
			HighFlowPenalty.Text = "1.0";
			RHO.Text = "0";
			MEKMaxFlow.Text = "1.00";
			MEKMinFlow.Text = "0.35";
			SkipPredictions.Text = "100";
		}

		private void DefaultParams7PlusBtn_Click(Object sender, EventArgs e) {
			AlwaysANN.Checked = true;
			UsingCorrectedPredictions.Checked = true;
			ConcentrationSP.Text = "95.3";
			CorrectionsToAverage.Text = "120";
			DampeningLimit.Text = "0.1";
			LowFlowPenalty.Text = "1.0";
			HighFlowPenalty.Text = "1.0";
			RHO.Text = "10";
			MEKMaxFlow.Text = "0.7615";
			MEKMinFlow.Text = "0.20";
			SkipPredictions.Text = "100";
		}

		private void DefaultParams13PlusBtn_Click(Object sender, EventArgs e) {
			DefaultParams7PlusBtn_Click(sender, e); // For now, use 7+ settings
		}

		private void ParamChanged(Object sender, EventArgs e) {
			StatusLabel.Text = "Params Changed, Ready for Writing...";
		}
	}

	public static class AdsExtensions
	{
		public static object Read(this TcAdsClient client, string symb, Type type) {
			return client.ReadSymbol(symb, type, false);
		}

		public static void Write(this TcAdsClient client, string symb, object val) {
			client.WriteSymbol(symb, val, false);
		}
	}
}