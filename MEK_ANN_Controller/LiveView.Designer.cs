﻿namespace MEK_ANN_Controller
{
    partial class LiveView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
				if (ADS != null) ADS.Dispose();
				if (RequestStream != null) RequestStream.Dispose();
				if (RecieveStream != null) RecieveStream.Dispose();

				components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			this.PlotContainer = new System.Windows.Forms.Panel();
			this.ManualReadBtn = new System.Windows.Forms.Button();
			this.LiveUpdateBtn = new System.Windows.Forms.CheckBox();
			this.PLCConnectedBtn = new System.Windows.Forms.CheckBox();
			this.SeriesVisibilityOptionsList = new System.Windows.Forms.CheckedListBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.CopyDataBtn = new System.Windows.Forms.CheckBox();
			this.SaveToFileBtn = new System.Windows.Forms.Button();
			this.ClearCSVDataBtn = new System.Windows.Forms.Button();
			this.CSVData = new System.Windows.Forms.TextBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.SimNeuralNetBtn = new System.Windows.Forms.Button();
			this.GetANNDataBtn = new System.Windows.Forms.CheckBox();
			this.TCLabel = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// PlotContainer
			// 
			this.PlotContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.PlotContainer.Location = new System.Drawing.Point(4, 33);
			this.PlotContainer.Name = "PlotContainer";
			this.PlotContainer.Size = new System.Drawing.Size(971, 572);
			this.PlotContainer.TabIndex = 0;
			// 
			// ManualReadBtn
			// 
			this.ManualReadBtn.Location = new System.Drawing.Point(4, 4);
			this.ManualReadBtn.Name = "ManualReadBtn";
			this.ManualReadBtn.Size = new System.Drawing.Size(157, 23);
			this.ManualReadBtn.TabIndex = 1;
			this.ManualReadBtn.Text = "Manual Update";
			this.toolTip1.SetToolTip(this.ManualReadBtn, "If you\'re not Live Updating, click here to update the PLC once");
			this.ManualReadBtn.UseVisualStyleBackColor = true;
			this.ManualReadBtn.Click += new System.EventHandler(this.ManualReadBtn_Click);
			// 
			// LiveUpdateBtn
			// 
			this.LiveUpdateBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.LiveUpdateBtn.Appearance = System.Windows.Forms.Appearance.Button;
			this.LiveUpdateBtn.Location = new System.Drawing.Point(808, 3);
			this.LiveUpdateBtn.Name = "LiveUpdateBtn";
			this.LiveUpdateBtn.Size = new System.Drawing.Size(167, 24);
			this.LiveUpdateBtn.TabIndex = 3;
			this.LiveUpdateBtn.Text = "Live Updating From PLC...";
			this.LiveUpdateBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.LiveUpdateBtn, "If On, we\'re periodically checking the PLC for new data and updating the UI with " +
        "it");
			this.LiveUpdateBtn.UseVisualStyleBackColor = true;
			this.LiveUpdateBtn.CheckedChanged += new System.EventHandler(this.LiveUpdateBtn_CheckedChanged);
			this.LiveUpdateBtn.Click += new System.EventHandler(this.LiveUpdateBtn_Click);
			// 
			// PLCConnectedBtn
			// 
			this.PLCConnectedBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.PLCConnectedBtn.Appearance = System.Windows.Forms.Appearance.Button;
			this.PLCConnectedBtn.AutoCheck = false;
			this.PLCConnectedBtn.Location = new System.Drawing.Point(685, 3);
			this.PLCConnectedBtn.Name = "PLCConnectedBtn";
			this.PLCConnectedBtn.Size = new System.Drawing.Size(117, 24);
			this.PLCConnectedBtn.TabIndex = 4;
			this.PLCConnectedBtn.Text = "Connected To PLC";
			this.PLCConnectedBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.PLCConnectedBtn, "If On, we\'re connected to an EM PLC with EM MEK ANN Tags. If not true, we\'re usin" +
        "g simulation data.");
			this.PLCConnectedBtn.UseVisualStyleBackColor = true;
			this.PLCConnectedBtn.CheckedChanged += new System.EventHandler(this.LiveUpdateBtn_CheckedChanged);
			// 
			// SeriesVisibilityOptionsList
			// 
			this.SeriesVisibilityOptionsList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.SeriesVisibilityOptionsList.CheckOnClick = true;
			this.SeriesVisibilityOptionsList.FormattingEnabled = true;
			this.SeriesVisibilityOptionsList.Location = new System.Drawing.Point(4, 611);
			this.SeriesVisibilityOptionsList.MultiColumn = true;
			this.SeriesVisibilityOptionsList.Name = "SeriesVisibilityOptionsList";
			this.SeriesVisibilityOptionsList.Size = new System.Drawing.Size(971, 34);
			this.SeriesVisibilityOptionsList.TabIndex = 5;
			this.toolTip1.SetToolTip(this.SeriesVisibilityOptionsList, "Hide/Show series on the chart above. Shift Click to Deselect All Predictions or S" +
        "elect only 1 Prediction (Clicked)");
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.CopyDataBtn);
			this.groupBox1.Controls.Add(this.SaveToFileBtn);
			this.groupBox1.Controls.Add(this.ClearCSVDataBtn);
			this.groupBox1.Controls.Add(this.CSVData);
			this.groupBox1.Location = new System.Drawing.Point(4, 651);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(971, 107);
			this.groupBox1.TabIndex = 6;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Data Saving";
			// 
			// CopyDataBtn
			// 
			this.CopyDataBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.CopyDataBtn.Appearance = System.Windows.Forms.Appearance.Button;
			this.CopyDataBtn.Location = new System.Drawing.Point(867, 16);
			this.CopyDataBtn.Name = "CopyDataBtn";
			this.CopyDataBtn.Size = new System.Drawing.Size(102, 24);
			this.CopyDataBtn.TabIndex = 7;
			this.CopyDataBtn.Text = "Copying Data";
			this.CopyDataBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.CopyDataBtn, "If On, data will be copied here when we update our data from the PLC. You can the" +
        "n save it using the Save Data button.");
			this.CopyDataBtn.UseVisualStyleBackColor = true;
			this.CopyDataBtn.CheckedChanged += new System.EventHandler(this.CopyCSVBtn_CheckedChanged);
			// 
			// SaveToFileBtn
			// 
			this.SaveToFileBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.SaveToFileBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.SaveToFileBtn.Location = new System.Drawing.Point(867, 80);
			this.SaveToFileBtn.Name = "SaveToFileBtn";
			this.SaveToFileBtn.Size = new System.Drawing.Size(102, 23);
			this.SaveToFileBtn.TabIndex = 2;
			this.SaveToFileBtn.Text = "Save Data...";
			this.toolTip1.SetToolTip(this.SaveToFileBtn, "Save this CSV to a file...");
			this.SaveToFileBtn.UseVisualStyleBackColor = true;
			this.SaveToFileBtn.Click += new System.EventHandler(this.SaveToFileBtn_Click);
			// 
			// ClearCSVDataBtn
			// 
			this.ClearCSVDataBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.ClearCSVDataBtn.Location = new System.Drawing.Point(867, 48);
			this.ClearCSVDataBtn.Name = "ClearCSVDataBtn";
			this.ClearCSVDataBtn.Size = new System.Drawing.Size(102, 23);
			this.ClearCSVDataBtn.TabIndex = 1;
			this.ClearCSVDataBtn.Text = "Clear Data";
			this.toolTip1.SetToolTip(this.ClearCSVDataBtn, "Start over this CSV aggregation history.");
			this.ClearCSVDataBtn.UseVisualStyleBackColor = true;
			this.ClearCSVDataBtn.Click += new System.EventHandler(this.ClearCSVDataBtn_Click);
			// 
			// CSVData
			// 
			this.CSVData.AcceptsReturn = true;
			this.CSVData.AcceptsTab = true;
			this.CSVData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.CSVData.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.CSVData.Location = new System.Drawing.Point(4, 16);
			this.CSVData.Multiline = true;
			this.CSVData.Name = "CSVData";
			this.CSVData.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.CSVData.Size = new System.Drawing.Size(856, 86);
			this.CSVData.TabIndex = 0;
			this.CSVData.Text = "1";
			this.CSVData.WordWrap = false;
			// 
			// SimNeuralNetBtn
			// 
			this.SimNeuralNetBtn.Location = new System.Drawing.Point(167, 4);
			this.SimNeuralNetBtn.Name = "SimNeuralNetBtn";
			this.SimNeuralNetBtn.Size = new System.Drawing.Size(157, 23);
			this.SimNeuralNetBtn.TabIndex = 7;
			this.SimNeuralNetBtn.Text = "Sim neural net";
			this.SimNeuralNetBtn.UseVisualStyleBackColor = true;
			this.SimNeuralNetBtn.Click += new System.EventHandler(this.SimNeuralNetBtn_Click);
			// 
			// GetANNDataBtn
			// 
			this.GetANNDataBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.GetANNDataBtn.Appearance = System.Windows.Forms.Appearance.Button;
			this.GetANNDataBtn.Location = new System.Drawing.Point(330, 3);
			this.GetANNDataBtn.Name = "GetANNDataBtn";
			this.GetANNDataBtn.Size = new System.Drawing.Size(167, 24);
			this.GetANNDataBtn.TabIndex = 8;
			this.GetANNDataBtn.Text = "Dump Next ANN Result...";
			this.GetANNDataBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.GetANNDataBtn.UseVisualStyleBackColor = true;
			this.GetANNDataBtn.CheckedChanged += new System.EventHandler(this.GetANNDataBtn_CheckedChanged);
			// 
			// TCLabel
			// 
			this.TCLabel.Location = new System.Drawing.Point(619, 9);
			this.TCLabel.Name = "TCLabel";
			this.TCLabel.Size = new System.Drawing.Size(60, 13);
			this.TCLabel.TabIndex = 19;
			this.TCLabel.Text = "TwinCat";
			this.TCLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// LiveView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(979, 761);
			this.Controls.Add(this.TCLabel);
			this.Controls.Add(this.GetANNDataBtn);
			this.Controls.Add(this.SimNeuralNetBtn);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.SeriesVisibilityOptionsList);
			this.Controls.Add(this.PLCConnectedBtn);
			this.Controls.Add(this.LiveUpdateBtn);
			this.Controls.Add(this.ManualReadBtn);
			this.Controls.Add(this.PlotContainer);
			this.Name = "LiveView";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "LiveView";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LiveView_FormClosing);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PlotContainer;
        private System.Windows.Forms.Button ManualReadBtn;
        private System.Windows.Forms.CheckBox LiveUpdateBtn;
        private System.Windows.Forms.CheckBox PLCConnectedBtn;
        private System.Windows.Forms.CheckedListBox SeriesVisibilityOptionsList;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button SaveToFileBtn;
        private System.Windows.Forms.Button ClearCSVDataBtn;
        private System.Windows.Forms.TextBox CSVData;
        private System.Windows.Forms.CheckBox CopyDataBtn;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button SimNeuralNetBtn;
		private System.Windows.Forms.CheckBox GetANNDataBtn;
		private System.Windows.Forms.Label TCLabel;
	}
}