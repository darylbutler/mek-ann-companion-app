﻿namespace MEK_ANN_Controller
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			this.LoadBtn = new System.Windows.Forms.Button();
			this.L1FlowWeightsGrid = new System.Windows.Forms.DataGridView();
			this.L1ConcWeightsGrid = new System.Windows.Forms.DataGridView();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.L1BiasesGrid = new System.Windows.Forms.DataGridView();
			this.L2WeightsGrid = new System.Windows.Forms.DataGridView();
			this.label4 = new System.Windows.Forms.Label();
			this.L2BiasGrid = new System.Windows.Forms.DataGridView();
			this.label5 = new System.Windows.Forms.Label();
			this.WriteToPLCBtn = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.NormFlow_yMin = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.NormFlow_xGain = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.NormFlow_xOffset = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.NormConc_yMin = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.NormConc_xGain = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.NormConc_xOffset = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.UnnormConc_yMin = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.UnnormConc_xGain = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.UnnormConc_xOffset = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.ReadFromPLCBtn = new System.Windows.Forms.Button();
			this.MEKMinFlow = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.MEKMaxFlow = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.LiveViewBtn = new System.Windows.Forms.Button();
			this.TCLabel = new System.Windows.Forms.Label();
			this.StatusLabel = new System.Windows.Forms.Label();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.label24 = new System.Windows.Forms.Label();
			this.DefaultParams13PlusBtn = new System.Windows.Forms.Button();
			this.DefaultParams7PlusBtn = new System.Windows.Forms.Button();
			this.DefaultParams6Minus = new System.Windows.Forms.Button();
			this.label23 = new System.Windows.Forms.Label();
			this.CorrectionsToAverage = new System.Windows.Forms.TextBox();
			this.ConcentrationSP = new System.Windows.Forms.TextBox();
			this.label22 = new System.Windows.Forms.Label();
			this.AlwaysANN = new System.Windows.Forms.CheckBox();
			this.SkipPredictions = new System.Windows.Forms.TextBox();
			this.label21 = new System.Windows.Forms.Label();
			this.UsingCorrectedPredictions = new System.Windows.Forms.CheckBox();
			this.HighFlowPenalty = new System.Windows.Forms.TextBox();
			this.label19 = new System.Windows.Forms.Label();
			this.LowFlowPenalty = new System.Windows.Forms.TextBox();
			this.label20 = new System.Windows.Forms.Label();
			this.RHO = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.DampeningLimit = new System.Windows.Forms.TextBox();
			this.label18 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.L1FlowWeightsGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L1ConcWeightsGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L1BiasesGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L2WeightsGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L2BiasGrid)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.SuspendLayout();
			// 
			// LoadBtn
			// 
			this.LoadBtn.Location = new System.Drawing.Point(5, 5);
			this.LoadBtn.Name = "LoadBtn";
			this.LoadBtn.Size = new System.Drawing.Size(127, 23);
			this.LoadBtn.TabIndex = 0;
			this.LoadBtn.Text = "Load Weights...";
			this.LoadBtn.UseVisualStyleBackColor = true;
			this.LoadBtn.Click += new System.EventHandler(this.LoadBtn_Click);
			// 
			// L1FlowWeightsGrid
			// 
			this.L1FlowWeightsGrid.AllowUserToAddRows = false;
			this.L1FlowWeightsGrid.AllowUserToDeleteRows = false;
			this.L1FlowWeightsGrid.AllowUserToResizeColumns = false;
			this.L1FlowWeightsGrid.AllowUserToResizeRows = false;
			this.L1FlowWeightsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.L1FlowWeightsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
			this.L1FlowWeightsGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
			this.L1FlowWeightsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.L1FlowWeightsGrid.DefaultCellStyle = dataGridViewCellStyle1;
			this.L1FlowWeightsGrid.Location = new System.Drawing.Point(5, 135);
			this.L1FlowWeightsGrid.MultiSelect = false;
			this.L1FlowWeightsGrid.Name = "L1FlowWeightsGrid";
			this.L1FlowWeightsGrid.ReadOnly = true;
			this.L1FlowWeightsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.L1FlowWeightsGrid.Size = new System.Drawing.Size(1034, 180);
			this.L1FlowWeightsGrid.TabIndex = 1;
			// 
			// L1ConcWeightsGrid
			// 
			this.L1ConcWeightsGrid.AllowUserToAddRows = false;
			this.L1ConcWeightsGrid.AllowUserToDeleteRows = false;
			this.L1ConcWeightsGrid.AllowUserToResizeColumns = false;
			this.L1ConcWeightsGrid.AllowUserToResizeRows = false;
			this.L1ConcWeightsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.L1ConcWeightsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.L1ConcWeightsGrid.Location = new System.Drawing.Point(5, 333);
			this.L1ConcWeightsGrid.Name = "L1ConcWeightsGrid";
			this.L1ConcWeightsGrid.Size = new System.Drawing.Size(1034, 180);
			this.L1ConcWeightsGrid.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(2, 119);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(86, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "L1 Flow Weights";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(2, 317);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(130, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "L1 Concentration Weights";
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(2, 516);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(53, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "L1 Biases";
			// 
			// L1BiasesGrid
			// 
			this.L1BiasesGrid.AllowUserToAddRows = false;
			this.L1BiasesGrid.AllowUserToDeleteRows = false;
			this.L1BiasesGrid.AllowUserToResizeColumns = false;
			this.L1BiasesGrid.AllowUserToResizeRows = false;
			this.L1BiasesGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.L1BiasesGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
			this.L1BiasesGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
			this.L1BiasesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.L1BiasesGrid.DefaultCellStyle = dataGridViewCellStyle2;
			this.L1BiasesGrid.Location = new System.Drawing.Point(5, 532);
			this.L1BiasesGrid.MultiSelect = false;
			this.L1BiasesGrid.Name = "L1BiasesGrid";
			this.L1BiasesGrid.ReadOnly = true;
			this.L1BiasesGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.L1BiasesGrid.Size = new System.Drawing.Size(165, 166);
			this.L1BiasesGrid.TabIndex = 6;
			// 
			// L2WeightsGrid
			// 
			this.L2WeightsGrid.AllowUserToAddRows = false;
			this.L2WeightsGrid.AllowUserToDeleteRows = false;
			this.L2WeightsGrid.AllowUserToResizeColumns = false;
			this.L2WeightsGrid.AllowUserToResizeRows = false;
			this.L2WeightsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.L2WeightsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
			this.L2WeightsGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
			this.L2WeightsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.L2WeightsGrid.DefaultCellStyle = dataGridViewCellStyle3;
			this.L2WeightsGrid.Location = new System.Drawing.Point(176, 532);
			this.L2WeightsGrid.MultiSelect = false;
			this.L2WeightsGrid.Name = "L2WeightsGrid";
			this.L2WeightsGrid.ReadOnly = true;
			this.L2WeightsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.L2WeightsGrid.Size = new System.Drawing.Size(692, 166);
			this.L2WeightsGrid.TabIndex = 8;
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(173, 516);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(61, 13);
			this.label4.TabIndex = 7;
			this.label4.Text = "L2 Weights";
			// 
			// L2BiasGrid
			// 
			this.L2BiasGrid.AllowUserToAddRows = false;
			this.L2BiasGrid.AllowUserToDeleteRows = false;
			this.L2BiasGrid.AllowUserToResizeColumns = false;
			this.L2BiasGrid.AllowUserToResizeRows = false;
			this.L2BiasGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.L2BiasGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
			this.L2BiasGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
			this.L2BiasGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.L2BiasGrid.DefaultCellStyle = dataGridViewCellStyle4;
			this.L2BiasGrid.Location = new System.Drawing.Point(874, 532);
			this.L2BiasGrid.MultiSelect = false;
			this.L2BiasGrid.Name = "L2BiasGrid";
			this.L2BiasGrid.ReadOnly = true;
			this.L2BiasGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.L2BiasGrid.Size = new System.Drawing.Size(165, 166);
			this.L2BiasGrid.TabIndex = 10;
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(871, 516);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(42, 13);
			this.label5.TabIndex = 9;
			this.label5.Text = "L2 Bias";
			// 
			// WriteToPLCBtn
			// 
			this.WriteToPLCBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.WriteToPLCBtn.Location = new System.Drawing.Point(912, 5);
			this.WriteToPLCBtn.Name = "WriteToPLCBtn";
			this.WriteToPLCBtn.Size = new System.Drawing.Size(127, 23);
			this.WriteToPLCBtn.TabIndex = 11;
			this.WriteToPLCBtn.Text = "Send To PLC";
			this.WriteToPLCBtn.UseVisualStyleBackColor = true;
			this.WriteToPLCBtn.Click += new System.EventHandler(this.WriteToPLCBtn_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.NormFlow_yMin);
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.NormFlow_xGain);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.NormFlow_xOffset);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Location = new System.Drawing.Point(898, 49);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(140, 80);
			this.groupBox1.TabIndex = 13;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Normalize Flow";
			// 
			// NormFlow_yMin
			// 
			this.NormFlow_yMin.Location = new System.Drawing.Point(45, 56);
			this.NormFlow_yMin.Name = "NormFlow_yMin";
			this.NormFlow_yMin.Size = new System.Drawing.Size(90, 20);
			this.NormFlow_yMin.TabIndex = 5;
			this.NormFlow_yMin.Text = "-1.0";
			this.NormFlow_yMin.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(5, 59);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(29, 13);
			this.label8.TabIndex = 4;
			this.label8.Text = "yMin";
			// 
			// NormFlow_xGain
			// 
			this.NormFlow_xGain.Location = new System.Drawing.Point(45, 35);
			this.NormFlow_xGain.Name = "NormFlow_xGain";
			this.NormFlow_xGain.Size = new System.Drawing.Size(90, 20);
			this.NormFlow_xGain.TabIndex = 3;
			this.NormFlow_xGain.Text = "1.2503052440522";
			this.NormFlow_xGain.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(5, 38);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(34, 13);
			this.label7.TabIndex = 2;
			this.label7.Text = "xGain";
			// 
			// NormFlow_xOffset
			// 
			this.NormFlow_xOffset.Location = new System.Drawing.Point(45, 14);
			this.NormFlow_xOffset.Name = "NormFlow_xOffset";
			this.NormFlow_xOffset.Size = new System.Drawing.Size(90, 20);
			this.NormFlow_xOffset.TabIndex = 1;
			this.NormFlow_xOffset.Text = "0.000390637";
			this.NormFlow_xOffset.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(5, 17);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(40, 13);
			this.label6.TabIndex = 0;
			this.label6.Text = "xOffset";
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.NormConc_yMin);
			this.groupBox2.Controls.Add(this.label9);
			this.groupBox2.Controls.Add(this.NormConc_xGain);
			this.groupBox2.Controls.Add(this.label10);
			this.groupBox2.Controls.Add(this.NormConc_xOffset);
			this.groupBox2.Controls.Add(this.label11);
			this.groupBox2.Location = new System.Drawing.Point(752, 49);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(140, 80);
			this.groupBox2.TabIndex = 14;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Normalize Conc";
			// 
			// NormConc_yMin
			// 
			this.NormConc_yMin.Location = new System.Drawing.Point(45, 56);
			this.NormConc_yMin.Name = "NormConc_yMin";
			this.NormConc_yMin.Size = new System.Drawing.Size(90, 20);
			this.NormConc_yMin.TabIndex = 5;
			this.NormConc_yMin.Text = "-1.0";
			this.NormConc_yMin.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(5, 59);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(29, 13);
			this.label9.TabIndex = 4;
			this.label9.Text = "yMin";
			// 
			// NormConc_xGain
			// 
			this.NormConc_xGain.Location = new System.Drawing.Point(45, 35);
			this.NormConc_xGain.Name = "NormConc_xGain";
			this.NormConc_xGain.Size = new System.Drawing.Size(90, 20);
			this.NormConc_xGain.TabIndex = 3;
			this.NormConc_xGain.Text = "0.383778312453459";
			this.NormConc_xGain.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(5, 38);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(34, 13);
			this.label10.TabIndex = 2;
			this.label10.Text = "xGain";
			// 
			// NormConc_xOffset
			// 
			this.NormConc_xOffset.Location = new System.Drawing.Point(45, 14);
			this.NormConc_xOffset.Name = "NormConc_xOffset";
			this.NormConc_xOffset.Size = new System.Drawing.Size(90, 20);
			this.NormConc_xOffset.TabIndex = 1;
			this.NormConc_xOffset.Text = "94.7886581";
			this.NormConc_xOffset.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(5, 17);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(40, 13);
			this.label11.TabIndex = 0;
			this.label11.Text = "xOffset";
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.UnnormConc_yMin);
			this.groupBox3.Controls.Add(this.label12);
			this.groupBox3.Controls.Add(this.UnnormConc_xGain);
			this.groupBox3.Controls.Add(this.label13);
			this.groupBox3.Controls.Add(this.UnnormConc_xOffset);
			this.groupBox3.Controls.Add(this.label14);
			this.groupBox3.Location = new System.Drawing.Point(606, 49);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(140, 80);
			this.groupBox3.TabIndex = 15;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Un-Normalize Conc";
			// 
			// UnnormConc_yMin
			// 
			this.UnnormConc_yMin.Location = new System.Drawing.Point(45, 56);
			this.UnnormConc_yMin.Name = "UnnormConc_yMin";
			this.UnnormConc_yMin.Size = new System.Drawing.Size(90, 20);
			this.UnnormConc_yMin.TabIndex = 5;
			this.UnnormConc_yMin.Text = "-1.0";
			this.UnnormConc_yMin.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(5, 59);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(29, 13);
			this.label12.TabIndex = 4;
			this.label12.Text = "yMin";
			// 
			// UnnormConc_xGain
			// 
			this.UnnormConc_xGain.Location = new System.Drawing.Point(45, 35);
			this.UnnormConc_xGain.Name = "UnnormConc_xGain";
			this.UnnormConc_xGain.Size = new System.Drawing.Size(90, 20);
			this.UnnormConc_xGain.TabIndex = 3;
			this.UnnormConc_xGain.Text = "0.383778312453459";
			this.UnnormConc_xGain.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(5, 38);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(34, 13);
			this.label13.TabIndex = 2;
			this.label13.Text = "xGain";
			// 
			// UnnormConc_xOffset
			// 
			this.UnnormConc_xOffset.Location = new System.Drawing.Point(45, 14);
			this.UnnormConc_xOffset.Name = "UnnormConc_xOffset";
			this.UnnormConc_xOffset.Size = new System.Drawing.Size(90, 20);
			this.UnnormConc_xOffset.TabIndex = 1;
			this.UnnormConc_xOffset.Text = "94.7886581";
			this.UnnormConc_xOffset.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(5, 17);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(40, 13);
			this.label14.TabIndex = 0;
			this.label14.Text = "xOffset";
			// 
			// ReadFromPLCBtn
			// 
			this.ReadFromPLCBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.ReadFromPLCBtn.Location = new System.Drawing.Point(779, 5);
			this.ReadFromPLCBtn.Name = "ReadFromPLCBtn";
			this.ReadFromPLCBtn.Size = new System.Drawing.Size(127, 23);
			this.ReadFromPLCBtn.TabIndex = 16;
			this.ReadFromPLCBtn.Text = "Read From PLC";
			this.ReadFromPLCBtn.UseVisualStyleBackColor = true;
			this.ReadFromPLCBtn.Click += new System.EventHandler(this.ReadFromPLCBtn_Click);
			// 
			// MEKMinFlow
			// 
			this.MEKMinFlow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.MEKMinFlow.Location = new System.Drawing.Point(439, 35);
			this.MEKMinFlow.Name = "MEKMinFlow";
			this.MEKMinFlow.Size = new System.Drawing.Size(30, 20);
			this.MEKMinFlow.TabIndex = 3;
			this.MEKMinFlow.Text = "0.0";
			this.MEKMinFlow.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label16
			// 
			this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(384, 38);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(49, 13);
			this.label16.TabIndex = 2;
			this.label16.Text = "Min Flow";
			// 
			// MEKMaxFlow
			// 
			this.MEKMaxFlow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.MEKMaxFlow.Location = new System.Drawing.Point(439, 14);
			this.MEKMaxFlow.Name = "MEKMaxFlow";
			this.MEKMaxFlow.Size = new System.Drawing.Size(30, 20);
			this.MEKMaxFlow.TabIndex = 1;
			this.MEKMaxFlow.Text = "0.25";
			this.MEKMaxFlow.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label17
			// 
			this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(384, 17);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(52, 13);
			this.label17.TabIndex = 0;
			this.label17.Text = "Max Flow";
			// 
			// LiveViewBtn
			// 
			this.LiveViewBtn.Location = new System.Drawing.Point(138, 5);
			this.LiveViewBtn.Name = "LiveViewBtn";
			this.LiveViewBtn.Size = new System.Drawing.Size(161, 23);
			this.LiveViewBtn.TabIndex = 17;
			this.LiveViewBtn.Text = "Live View...";
			this.LiveViewBtn.UseVisualStyleBackColor = true;
			this.LiveViewBtn.Click += new System.EventHandler(this.LiveViewBtn_Click);
			// 
			// TCLabel
			// 
			this.TCLabel.Location = new System.Drawing.Point(713, 10);
			this.TCLabel.Name = "TCLabel";
			this.TCLabel.Size = new System.Drawing.Size(60, 13);
			this.TCLabel.TabIndex = 18;
			this.TCLabel.Text = "TwinCat";
			this.TCLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// StatusLabel
			// 
			this.StatusLabel.Location = new System.Drawing.Point(678, 31);
			this.StatusLabel.Name = "StatusLabel";
			this.StatusLabel.Size = new System.Drawing.Size(360, 17);
			this.StatusLabel.TabIndex = 19;
			this.StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// groupBox5
			// 
			this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox5.Controls.Add(this.label24);
			this.groupBox5.Controls.Add(this.DefaultParams13PlusBtn);
			this.groupBox5.Controls.Add(this.DefaultParams7PlusBtn);
			this.groupBox5.Controls.Add(this.DefaultParams6Minus);
			this.groupBox5.Controls.Add(this.label23);
			this.groupBox5.Controls.Add(this.CorrectionsToAverage);
			this.groupBox5.Controls.Add(this.ConcentrationSP);
			this.groupBox5.Controls.Add(this.label22);
			this.groupBox5.Controls.Add(this.AlwaysANN);
			this.groupBox5.Controls.Add(this.label16);
			this.groupBox5.Controls.Add(this.MEKMinFlow);
			this.groupBox5.Controls.Add(this.SkipPredictions);
			this.groupBox5.Controls.Add(this.label21);
			this.groupBox5.Controls.Add(this.MEKMaxFlow);
			this.groupBox5.Controls.Add(this.label17);
			this.groupBox5.Controls.Add(this.UsingCorrectedPredictions);
			this.groupBox5.Controls.Add(this.HighFlowPenalty);
			this.groupBox5.Controls.Add(this.label19);
			this.groupBox5.Controls.Add(this.LowFlowPenalty);
			this.groupBox5.Controls.Add(this.label20);
			this.groupBox5.Controls.Add(this.RHO);
			this.groupBox5.Controls.Add(this.label15);
			this.groupBox5.Controls.Add(this.DampeningLimit);
			this.groupBox5.Controls.Add(this.label18);
			this.groupBox5.Location = new System.Drawing.Point(125, 49);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(475, 80);
			this.groupBox5.TabIndex = 17;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Parameters";
			// 
			// label24
			// 
			this.label24.Location = new System.Drawing.Point(1, 38);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(45, 40);
			this.label24.TabIndex = 20;
			this.label24.Text = "Load Params";
			this.label24.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// DefaultParams13PlusBtn
			// 
			this.DefaultParams13PlusBtn.Location = new System.Drawing.Point(116, 54);
			this.DefaultParams13PlusBtn.Name = "DefaultParams13PlusBtn";
			this.DefaultParams13PlusBtn.Size = new System.Drawing.Size(34, 23);
			this.DefaultParams13PlusBtn.TabIndex = 22;
			this.DefaultParams13PlusBtn.Text = "13+";
			this.DefaultParams13PlusBtn.UseVisualStyleBackColor = true;
			this.DefaultParams13PlusBtn.Click += new System.EventHandler(this.DefaultParams13PlusBtn_Click);
			// 
			// DefaultParams7PlusBtn
			// 
			this.DefaultParams7PlusBtn.Location = new System.Drawing.Point(82, 54);
			this.DefaultParams7PlusBtn.Name = "DefaultParams7PlusBtn";
			this.DefaultParams7PlusBtn.Size = new System.Drawing.Size(34, 23);
			this.DefaultParams7PlusBtn.TabIndex = 21;
			this.DefaultParams7PlusBtn.Text = "7+";
			this.DefaultParams7PlusBtn.UseVisualStyleBackColor = true;
			this.DefaultParams7PlusBtn.Click += new System.EventHandler(this.DefaultParams7PlusBtn_Click);
			// 
			// DefaultParams6Minus
			// 
			this.DefaultParams6Minus.Location = new System.Drawing.Point(48, 54);
			this.DefaultParams6Minus.Name = "DefaultParams6Minus";
			this.DefaultParams6Minus.Size = new System.Drawing.Size(34, 23);
			this.DefaultParams6Minus.TabIndex = 20;
			this.DefaultParams6Minus.Text = "6-";
			this.DefaultParams6Minus.UseVisualStyleBackColor = true;
			this.DefaultParams6Minus.Click += new System.EventHandler(this.DefaultParams6Minus_Click);
			// 
			// label23
			// 
			this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(156, 38);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(67, 13);
			this.label23.TabIndex = 15;
			this.label23.Text = "Corr To Avg:";
			// 
			// CorrectionsToAverage
			// 
			this.CorrectionsToAverage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.CorrectionsToAverage.Location = new System.Drawing.Point(223, 35);
			this.CorrectionsToAverage.Name = "CorrectionsToAverage";
			this.CorrectionsToAverage.Size = new System.Drawing.Size(41, 20);
			this.CorrectionsToAverage.TabIndex = 14;
			this.CorrectionsToAverage.Text = "12";
			this.CorrectionsToAverage.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// ConcentrationSP
			// 
			this.ConcentrationSP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.ConcentrationSP.Location = new System.Drawing.Point(223, 14);
			this.ConcentrationSP.Name = "ConcentrationSP";
			this.ConcentrationSP.Size = new System.Drawing.Size(41, 20);
			this.ConcentrationSP.TabIndex = 13;
			this.ConcentrationSP.Text = "95.2";
			this.ConcentrationSP.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label22
			// 
			this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(156, 17);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(52, 13);
			this.label22.TabIndex = 12;
			this.label22.Text = "Conc SP:";
			// 
			// AlwaysANN
			// 
			this.AlwaysANN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.AlwaysANN.AutoSize = true;
			this.AlwaysANN.Location = new System.Drawing.Point(49, 16);
			this.AlwaysANN.Name = "AlwaysANN";
			this.AlwaysANN.Size = new System.Drawing.Size(85, 17);
			this.AlwaysANN.TabIndex = 11;
			this.AlwaysANN.Text = "Always ANN";
			this.AlwaysANN.UseVisualStyleBackColor = true;
			this.AlwaysANN.CheckedChanged += new System.EventHandler(this.ParamChanged);
			// 
			// SkipPredictions
			// 
			this.SkipPredictions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.SkipPredictions.Location = new System.Drawing.Point(439, 56);
			this.SkipPredictions.Name = "SkipPredictions";
			this.SkipPredictions.Size = new System.Drawing.Size(30, 20);
			this.SkipPredictions.TabIndex = 10;
			this.SkipPredictions.Text = "100";
			this.SkipPredictions.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label21
			// 
			this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(384, 59);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(50, 13);
			this.label21.TabIndex = 9;
			this.label21.Text = "Skip Cnt:";
			// 
			// UsingCorrectedPredictions
			// 
			this.UsingCorrectedPredictions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.UsingCorrectedPredictions.AutoSize = true;
			this.UsingCorrectedPredictions.Location = new System.Drawing.Point(49, 37);
			this.UsingCorrectedPredictions.Name = "UsingCorrectedPredictions";
			this.UsingCorrectedPredictions.Size = new System.Drawing.Size(96, 17);
			this.UsingCorrectedPredictions.TabIndex = 8;
			this.UsingCorrectedPredictions.Text = "Use Correction";
			this.UsingCorrectedPredictions.UseVisualStyleBackColor = true;
			this.UsingCorrectedPredictions.CheckedChanged += new System.EventHandler(this.ParamChanged);
			// 
			// HighFlowPenalty
			// 
			this.HighFlowPenalty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.HighFlowPenalty.Location = new System.Drawing.Point(337, 35);
			this.HighFlowPenalty.Name = "HighFlowPenalty";
			this.HighFlowPenalty.Size = new System.Drawing.Size(41, 20);
			this.HighFlowPenalty.TabIndex = 7;
			this.HighFlowPenalty.Text = "0.0";
			this.HighFlowPenalty.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label19
			// 
			this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(270, 38);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(63, 13);
			this.label19.TabIndex = 6;
			this.label19.Text = "H-Flow Mult";
			// 
			// LowFlowPenalty
			// 
			this.LowFlowPenalty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.LowFlowPenalty.Location = new System.Drawing.Point(337, 14);
			this.LowFlowPenalty.Name = "LowFlowPenalty";
			this.LowFlowPenalty.Size = new System.Drawing.Size(41, 20);
			this.LowFlowPenalty.TabIndex = 5;
			this.LowFlowPenalty.Text = "0.0";
			this.LowFlowPenalty.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label20
			// 
			this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(270, 17);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(61, 13);
			this.label20.TabIndex = 4;
			this.label20.Text = "L-Flow Mult";
			// 
			// RHO
			// 
			this.RHO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.RHO.Location = new System.Drawing.Point(337, 56);
			this.RHO.Name = "RHO";
			this.RHO.Size = new System.Drawing.Size(41, 20);
			this.RHO.TabIndex = 3;
			this.RHO.Text = "0.0";
			this.RHO.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label15
			// 
			this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(270, 59);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(31, 13);
			this.label15.TabIndex = 2;
			this.label15.Text = "RHO";
			// 
			// DampeningLimit
			// 
			this.DampeningLimit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.DampeningLimit.Location = new System.Drawing.Point(223, 56);
			this.DampeningLimit.Name = "DampeningLimit";
			this.DampeningLimit.Size = new System.Drawing.Size(41, 20);
			this.DampeningLimit.TabIndex = 1;
			this.DampeningLimit.Text = "0.001";
			this.DampeningLimit.TextChanged += new System.EventHandler(this.ParamChanged);
			// 
			// label18
			// 
			this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(156, 59);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(61, 13);
			this.label18.TabIndex = 0;
			this.label18.Text = "Dampening";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1043, 710);
			this.Controls.Add(this.groupBox5);
			this.Controls.Add(this.StatusLabel);
			this.Controls.Add(this.TCLabel);
			this.Controls.Add(this.LiveViewBtn);
			this.Controls.Add(this.ReadFromPLCBtn);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.WriteToPLCBtn);
			this.Controls.Add(this.L2BiasGrid);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.L2WeightsGrid);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.L1BiasesGrid);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.L1ConcWeightsGrid);
			this.Controls.Add(this.L1FlowWeightsGrid);
			this.Controls.Add(this.LoadBtn);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Load Data";
			((System.ComponentModel.ISupportInitialize)(this.L1FlowWeightsGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L1ConcWeightsGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L1BiasesGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L2WeightsGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L2BiasGrid)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button LoadBtn;
        private System.Windows.Forms.DataGridView L1FlowWeightsGrid;
        private System.Windows.Forms.DataGridView L1ConcWeightsGrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView L1BiasesGrid;
        private System.Windows.Forms.DataGridView L2WeightsGrid;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView L2BiasGrid;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button WriteToPLCBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox NormFlow_yMin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox NormFlow_xGain;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox NormFlow_xOffset;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox NormConc_yMin;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox NormConc_xGain;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox NormConc_xOffset;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox UnnormConc_yMin;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox UnnormConc_xGain;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox UnnormConc_xOffset;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button ReadFromPLCBtn;
        private System.Windows.Forms.TextBox MEKMinFlow;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox MEKMaxFlow;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button LiveViewBtn;
		private System.Windows.Forms.Label TCLabel;
		private System.Windows.Forms.Label StatusLabel;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.CheckBox UsingCorrectedPredictions;
		private System.Windows.Forms.TextBox HighFlowPenalty;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.TextBox LowFlowPenalty;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TextBox RHO;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox DampeningLimit;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.TextBox SkipPredictions;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.CheckBox AlwaysANN;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.TextBox ConcentrationSP;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.TextBox CorrectionsToAverage;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Button DefaultParams13PlusBtn;
		private System.Windows.Forms.Button DefaultParams7PlusBtn;
		private System.Windows.Forms.Button DefaultParams6Minus;
	}
}

